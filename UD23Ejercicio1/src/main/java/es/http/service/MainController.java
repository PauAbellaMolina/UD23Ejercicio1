package es.http.service;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;

@Controller
public class MainController {
	@RequestMapping("/")
    public String HelloWorld() {
        return "HelloWorld";
    }
	
	@RequestMapping("/whoMadeThis")
    public String WhoMadeThis() {
        return "IMadeThis";
    }
}
